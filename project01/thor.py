#!/usr/bin/env python2.7

import socket
import getopt
import logging
import time
import os
import sys

DOMAIN = 'www.example.com'
ADDRESS = '127.0.0.1'
PROGRAM = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO
PORT = 80
REQUESTS = 1
PROCESSES = 1

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} [-v] ADDRESS PORT
Usage: {program} [-v] ADDRESS PORT

Options:

    -h       Show this help message
    -v       Set logging to DEBUG level
    -r REQUESTS  Number of requests per process (default is 1)
    -p PROCESSES Number of processes (default is 1)
'''.format(port=PORT, program=PROGRAM)
	sys.exit(exit_code)



class TCPClient(object):

    def __init__(self, address=ADDRESS, port=PORT):
        ''' Construct TCPClient object with the specified address and port '''
        self.logger  = logging.getLogger() # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address    # Store address to listen on
        self.port    = port       # Store port to lisen on

    def handle(self):
        ''' Handle connection '''
        self.logger.debug('Handle')
        raise NotImplementedError

    def run(self):
        ''' Run client by connecting to specified address and port and then
        executing the handle method '''
        try:
            # Connect to server with specified address and port, create file object
            self.socket.connect((self.address, int(self.port)))
            self.stream = self.socket.makefile('w+')
        except socket.error as e:
            self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))

        # Run handle method and then the finish method
        try:
            self.handle()
        except Exception as e:
            self.logger.exception('Exception: {}', e)
        finally:
            self.finish()

    def finish(self):
        ''' Finish connection '''
        self.logger.debug('Finish')
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# EchoClient Class

class HTTPClient(TCPClient):
        
        def __init__(self, address, port, path):
            TCPClient.__init__(self, address, port)
            self.host = address
            self.path = path
	
	def handle(self):
        	''' Handle connection by reading data and then writing it back until EOF '''
		self.logger.debug('Handle')
		self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
		self.stream.write('Host: {}\r\n'.format(self.host))
		self.stream.write('\r\n')
		self.stream.flush()
		try:
			data = self.stream.readline()
			while data:
				# Send STDIN to Server
				sys.stdout.write(data)
				self.stream.flush()
				data = self.stream.readline()
		except socket.error:
		    pass    # Ignore socket errors

# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "hvp:r:")
	except getopt.GetoptError as e:
		usage(1)

	for option, value in options:
		if option == '-v':
			LOGLEVEL = logging.DEBUG
		elif option == '-p':
			PROCESSES = int(value)
		elif option == '-r':
			REQUESTS = int(value)
		else:
		    usage(1)

	if len(arguments) >= 1:
		ADDRESS = arguments[0]
	if len(arguments) >= 2:
		PORT    = int(arguments[1])
	# Parsing URL
	url = ADDRESS.split('://')[-1]

	print url
	if '/' not in url:
	    path = '/'
	else:
	    path = '/' + url.split('/', 1)[-1]
	    url = url.split('/',1)[0]

	print url
	if ':' not in url:
		port = 80
	else:
		port = url.split(':',1)[-1]
		url = url.split(':',1)[0]

	print url
	logging.basicConfig(
		level   = LOGLEVEL,
		format  = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
	)

# Lookup host address
	try:
		url = socket.gethostbyname(url)
	except socket.gaierror as e:
		logging.error('Unable to lookup {}: {}'.format(ADDRESS, e))
		sys.exit(1)

# Instantiate and run client

	for process in range(PROCESSES):
		elapsedtime = 0
		pid = os.fork
		for request in range(REQUESTS):
			start_time = time.time()

			client = HTTPClient(url, port,path)

			try:
				client.run()
			except KeyboardInterrupt:
				sys.exit(0)
			end_time = time.time()
			elapsedtime+=(end_time - start_time)
		logging.debug('Elapsed time: {:0.2f}'.format(elapsedtime/REQUESTS))
		if pid == 0:
			try:
				pid, status = os.wait()
			except OSError:
				os.wait()
				sys.exit(fork)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:







