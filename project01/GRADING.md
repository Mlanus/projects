Project 01 - Grading
====================

**Score**: 15 / 20

Deductions
----------

* Thor

    - 0.25  Doesn't handle no arguments gracefully
    - 1.0   Not fully concurrent
            You don't actually call `os.fork`, should fork all children and then wait
    - 0.5   Child needs to exit after all requests

* Spidey

    - 0.25  ADDRESS should be `0.0.0.0`
    - 0.25  Failed directory listing test
    - 0.5   Missing error checking of system calls (fork)
    - 1.0   Never actually uses forking mode (need to pass forking parameter to constructor)
            And the forking implementation is incorrect

* Report

    - 0.5   Report does not build (no Makefile)
    - 0.5   Missing experiment scripts
    - 0.25  Insufficient analysis

Comments
--------

* Should use logger for debugging rather than print
