#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys
import mimetypes
import signal

# Constants

ADDRESS  = 'student03.cse.nd.edu'
PORT     = 9234
BACKLOG  = 0
LOGLEVEL = logging.INFO
FORKING = 0
PROGRAM  = os.path.basename(sys.argv[0])
DOCROOT = '.'

# Utility Functions

def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {program} [-p PORT -v]

Options:

    -h       Show this help message
    -v       Set logging to DEBUG level
    -f       Enable forking mode
    
    -p PORT     TCP Port to listen to (default is {port})
    -d DOCROOT  Set root directory (default is current directory)
'''.format(port=PORT, program=PROGRAM)
    sys.exit(exit_code)

# BaseHandler Class

class BaseHandler(object):
    def __init__(self, fd, address):
    	self.socket = fd
    	self.address = address
    	self.stream = self.socket.makefile('w+')

    def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor

        self.debug('Connect')

    def debug(self, message, *args):
        ''' Convenience debugging function '''
        message = message.format(*args)
        self.logger.debug('{} | {}'.format(self.address, message))

    def info(self, message, *args):
        ''' Convenience information function '''
        message = message.format(*args)
        self.logger.info('{} | {}'.format(self.address, message))

    def warn(self, message, *args):
        ''' Convenience warning function '''
        message = message.format(*args)
        self.logger.warn('{} | {}'.format(self.address, message))

    def error(self, message, *args):
        ''' Convenience error function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def exception(self, message, *args):
        ''' Convenience exception function '''
        message = message.format(*args)
        self.logger.exception('{} | {}'.format(self.address, message))

    def handle(self):
        ''' Handle connection '''
        self.debug('Handle')
        raise NotImplementedError

    def finish(self):
        ''' Finish connection by flushing stream, shutting down socket, and
        then closing it '''
        self.debug('Finish')
        try:
            self.stream.flush()
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error as e:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTPHandler Class

class HTTPHandler(BaseHandler):
    
    def _handle_error(self, e):
        self.stream.write('HTTP/1.0')
        self.stream.write(e)
        self.stream.write('Error\r\n')
        self.stream.write('Content-type: text/html\r\n')
        self.stream.write('\r\n')
        self.stream.write('<html>')
        self.stream.write(e)
        self.stream.write('Error')
        self.stream.write('<img src="http://www.websitemagazine.com/images/blog/404-example.png" alt = "Oh No", style="width:600px;height:387;">')
        self.stream.flush()
        
    def _handle_script(self):
        signal.signal(signal.SIGCHLD, signal.SIG_DFL)
        for line in os.popen(self.uripath):
            self.stream.write(line)
        self.stream.flush()
        signal.signal(signal.SIGCHLD, signal.SIG_IGN)
        
        
    
    def _handle_file(self):
        mimetype, _ = mimetypes.guess_type(self.uripath)
        if mimetype is None:
            mimetype = 'application/octet-stream'
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-type:  {mimetype} \r\n\r\n')
        stream = open(self.uripath)
        for line in stream:
            self.stream.write(line)
        self.stream.flush()
        
    
    def _handle_directory(self):
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-type: text/html\r\n\r\n')
        self.stream.write('<html>')
        for path in sorted(os.listdir(self.uripath)):
            self.stream.write(path)
            self.stream.write("&nbsp&nbsp&nbsp")
            self.stream.write(os.path.getsize(path))
            self.stream.write("&nbsp&nbsp&nbsp")
            if os.path.isdir(path):
                self.stream.write("dir")
            else:
                self.stream.write("file")
            self.stream.write("<br>")
        self.stream.write("</html>")
        self.stream.flush()
    
    
    def _parse_request(self):
        i = 0
        try:
            data = self.stream.readline().rstrip()
            while data:
              #  print "IN LOOP"
                print data
                if i == 0:
                    s = data.split(" ")
                    os.environ['REQUEST_METHOD'] = s[0]
                    #print os.environ['REQUEST_METHOD']
                    if '?' in s[1]:
                        os.environ['QUERY_STRING'] = s[1].split('?')[1]
                        os.environ['REQUEST_URI'] = s[1].split('?')[0]
                    else: 
                        os.environ['QUERY_STRING'] = ''
                        os.environ['REQUEST_URI'] = s[1]
                    i += 1
                   # print os.environ['QUERY_STRING']
                   # print os.environ['REQUEST_URI']
                else:   
                    s = data.split(': ')
                    s[0].replace('-', '_')
                    s[0].upper()
                    s[0] = 'HTTP_'.join(s[0])
                    os.environ[s[0]] = s[1]
                data = self.stream.readline().rstrip()
                #if data == '\r\n':
                   #data = self.stream.readline()
        except socket.error:
            pass
        
    def handle(self):
       # print "HANDLING"
        ''' Handle connection by parsing HTTP request '''
        self._parse_request()
       # print "EXIT LOOP"
        #print DOCROOT
        #print DOCROOT + os.environ['REQUEST_URI']
        self.uripath = os.path.normpath(DOCROOT + os.environ['REQUEST_URI'])
        print "URIPATH IS ",
        print self.uripath

        if not os.path.exists(self.uripath):# or self.uripath.split('/')[0] != DOCROOT:
            print "Handling error --------"
            self._handle_error(404)
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.X_OK):
            print "Handling script -------"
            self._handle_script()
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.R_OK):
            print "Handling file ---------"
            self._handle_file()
        elif os.path.isdir(self.uripath):# and os.access(self.uripath, os.R_OK):
            print "Handling dir ----------"
            self._handle_directory()
        else:
            self._handler_error(403)
        
        
        

# TCPServer Class

class TCPServer(object):

    def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler, forking = FORKING, docroot = DOCROOT):
        ''' Construct TCPServer object with the specified address, port, and handler '''
        self.logger  = logging.getLogger() # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address    # Store address to listen on
        self.port    = port       # Store port to lisen on
        self.handler = handler    # Store handler for incoming connections
        self.forking = forking
        self.docroot = docroot

    def run(self):
        print "SERVER RUNNING"
        ''' Run TCP Server on specified address and port by calling the
        specified handler on each incoming connection '''
        try:
            # Bind socket to address and port and then listen
            self.socket.bind((self.address, self.port))
            self.socket.listen(BACKLOG)
        except socket.error as e:
            self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.info('Listening on {}:{}...'.format(self.address, self.port))

        while True:
            # Accept incoming connection
            client, address = self.socket.accept()
            self.logger.debug('Accepted connection from {}:{}'.format(*address))

            if not self.forking:
                try:
                    handler = self.handler(client, address)
                    handler.handle()
                except Exception as e:
                    handler.exception('Exception: {}', e)
                finally:
                    handler.finish()
            else:
                pid = os.fork()
                if pid == 0:
                    self.handle(client, address)
                    os._exit(0)
                else:
                    client.close()

# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hp:vfd:")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-p':
            PORT = int(value)
        elif option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-f':
            FORKING = 1
        elif option == '-d':
            DOCROOT = str(value)
        else:
            usage(1)

    # Set logging level
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

    # Instantiate and run server
    server = TCPServer(port=PORT)

    try:
        server.run()
    except KeyboardInterrupt:
        sys.exit(0)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

