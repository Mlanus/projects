#!/usr/bin/env python2.7

import sys
import string
import hashlib
import random
import itertools
import getopt

#Constants

ALPHABET = string.ascii_lowercase + string.digits
LENGTH   = 8
HASHES   = 'hashes.txt'
PREFIX = ''
PROGRAM = 'hulk.py'

# Help Message

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

    -h       Show this help message

    -a       Set password alphabet, default is a-z + 0-9
    -l LENGTH  Length of passwirds to be found (default is 8)
    -s HASHES  File where password hashes are located (default is hashes.txt)
	-p PREFIX  Return only passwords starting with this string (default is none)
'''.format(program=PROGRAM)
	sys.exit(exit_code)


# Utility function

def md5sum(s):
	return hashlib.md5(s).hexdigest()


# Main Execution

if __name__ == '__main__':
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "a:l:s:p:")
	except getopt.GetoptError as e:
		usage(1)

	for option, value in options:
		if option == '-a':
			ALPHABET = value
		elif option == '-l':
			LENGTH = int(value)
		elif option == '-s':
			HASHES = value
		elif option == '-p':
			PREFIX = value
		else:
		    usage(1)


	hashes = set([l.strip() for l in open(HASHES)])
	#found  = set()

	for candidate in itertools.product(ALPHABET, repeat = LENGTH):
		candidate = ''.join(map(str, candidate))
		candidate = PREFIX + candidate
		checksum = md5sum(candidate)
		if checksum in hashes:
			print candidate

