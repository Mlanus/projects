#!/usr/bin/env python2.7

import sys
import work_queue
import getopt
import json
import os
import itertools
import string

# Constants

ALPHABET = string.ascii_lowercase + string.digits
LENGTH = 8
HASHES = 'hashes.txt'
TASKS = 10 #int(sys.argv[1])
SOURCES = ('hulk.py', HASHES)
PREFIX = ''
PROGRAM = 'fury.py'
JOURNAL = {}
P_LENGTH = LENGTH - 6

# Read Journal
#f = open('journal.json', 'r')

# Help Message

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

    -h       Show this help message

    -a       Set password alphabet, default is a-z + 0-9
    -l LENGTH  Length of passwirds to be found (default is 8)
    -s HASHES  File where password hashes are located (default is hashes.txt)
	-p PREFIX  Return only passwords starting with this string (default is none)
'''.format(program=PROGRAM)
	sys.exit(exit_code)


# Main Execution

if __name__ == '__main__':
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "a:l:s:p:")
	except getopt.GetoptError as e:
		usage(1)

	for option, value in options:
		if option == '-a':
			ALPHABET = value
		elif option == '-l':
			LENGTH = int(value)
		elif option == '-s':
			HASHES = value
		elif option == '-p':
			PREFIX = value
		else:
		    usage(1)
	
	queue = work_queue.WorkQueue(work_queue.WORK_QUEUE_RANDOM_PORT, name='hulk-mflanag6', catalog=True)
	queue.specify_log('fury.log')
	
	for _ in range(TASKS):
		for prefix in itertools.product(ALPHABET, repeat = P_LENGTH):
			PREFIX = prefix[0]
			command = './hulk.py -l {} -s {} -p {}'.format(LENGTH-P_LENGTH, HASHES, PREFIX)
			task = work_queue.Task(command)
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
			queue.submit(task)

	while not queue.empty():
		task = queue.wait()
		if task and task.return_status == 0:
			sys.stdout.write(task.output)
			sys.stdout.flush()
			JOURNAL[task.command] = task.output.split('\n')
			with open('journal.json.new', 'w') as stream:
				json.dump(JOURNAL, stream)
		os.rename('journal.json.new', 'journal.json')


